
import transformers
import datasets
import random, sys, argparse, os, logging, torch

from transformers import AutoModelForSequenceClassification, AutoTokenizer, Trainer, TrainingArguments
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from datasets import load_from_disk

print(transformers.__version__)
print(datasets.__version__)

model_name = "distilbert-base-uncased"
epochs = 1
num_labels = 5

learning_rate = 5e-5
train_batch_size = 32
eval_batch_size = 64
save_strategy = 'no'
save_steps = 500

output_data_dir = "./output"
model_dir = "./model"
training_data_dir = "./data/amazon_shoe_reviews_train"
test_data_dir = "./data/amazon_shoe_reviews_test"

train_dataset = load_from_disk(training_data_dir)
valid_dataset = load_from_disk(test_data_dir)

print(len(train_dataset))
print(len(valid_dataset))

def compute_metrics(pred):
        labels = pred.label_ids
        preds = pred.predictions.argmax(-1)
        precision, recall, f1, _ = precision_recall_fscore_support(labels, preds)
        acc = accuracy_score(labels, preds)
        return {"accuracy": acc, "f1": f1, "precision": precision, "recall": recall}

model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=num_labels)
tokenizer = AutoTokenizer.from_pretrained(model_name)

def tokenize(batch):
        return tokenizer(batch['text'], padding='max_length', truncation=True)

train_dataset = train_dataset.map(tokenize, batched=True, batch_size=len(train_dataset))
valid_dataset = valid_dataset.map(tokenize, batched=True, batch_size=len(valid_dataset))

train_dataset = train_dataset.remove_columns(['text'])
valid_dataset = valid_dataset.remove_columns(['text'])

training_args = TrainingArguments(
        output_dir=model_dir,
        num_train_epochs=epochs,
        per_device_train_batch_size=train_batch_size,
        per_device_eval_batch_size=eval_batch_size,
        save_strategy=save_strategy,
        save_steps=save_steps,
        evaluation_strategy="epoch",
        logging_dir="./logs",
        learning_rate=learning_rate,
)

trainer = Trainer(
        model=model,
        args=training_args,
        tokenizer=tokenizer,
        compute_metrics=compute_metrics,
        train_dataset=train_dataset,
        eval_dataset=valid_dataset,
    )

trainer.train()

trainer.evaluate(eval_dataset=valid_dataset)

trainer.save_model(model_dir)
